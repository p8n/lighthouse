import React from 'react';
import { EditableText } from '@blueprintjs/core';

import Mnemonic from '../Mnemonic.js';

function cellRenderer({ columnIndex, key, rowIndex, style, parent }, listing, uuid, backend) {
  const line = listing[rowIndex];
  const myStyle = Object.assign({whiteSpace: "nowrap"}, style);

  let child;

  switch (columnIndex) {
    // Address
    case 0:
      child = (
        <Address
          key={key}
          style={myStyle}
          address={line.address}
          offset={line.offset}
          isHead={line.isHead}
          from={line.sources && line.sources.length > 0 ? line.sources[0] : ""}
          to={line.targets && line.targets.length > 0 ? line.targets[0] : ""}
        />
      );
      break;

    // Mnenonic
    case 1:
      child = (
        <Mnemonic
          key={key}
          style={myStyle}
          mnemonic={line}
          uuid={uuid}
          guard={line.guards && line.guards.length > 0 ? line.guards[0] : ""}
        />
      );
      break;

    // Comment
    case 2:
      child = (
        <Comment
          key={key}
          style={myStyle}
          comment={line.comment}
          backend={backend}
          address={line.address} />
      );
      break;

    default:
      console.error("Unknown column index", columnIndex);
  }

  return child;
}

function Address({ address, offset, style, from, to, isHead }) {
  const props = {
    'className': "Address",
    'data-shortend-address': (isHead ? address : offset),
    'data-full-address': address,
  };

  return (
    <div style={style} className="Listing-column Address-background">
      <div {...props} />
    </div>
  );
}

function Comment({ comment, address, style, backend }) {
  return (
    <div style={style} className="Listing-column">
      <div className="Comment">
        <EditableText
          defaultValue={comment}
          placeholder="Click here to comment"
          confirmOnEnterKey={true}
          onConfirm={(cmnt) => backend.comment(address, cmnt)} />
      </div>
    </div>
  );
}

export { cellRenderer };
