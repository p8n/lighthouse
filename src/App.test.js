import React from 'react';
import { shallow, mount, render } from 'enzyme';
import App from './App';
import Panopticon from 'panopticon-node';

const backend = {
    applyUpdates: () => false,
    functions: {
      state: 'present',
      value: {
        uuid1: {
          name: 'func1'
        }
      }
    }
  };

test('check basic elements', () => {
  const app = mount(<App backend={backend} />);

  expect(app.exists("Sidebar"))
  expect(app.exists("Nav"))
  expect(app.exists("NonIdealState"))
  app.unmount()
});
