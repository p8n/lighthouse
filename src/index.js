import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.js';
import registerServiceWorker from './registerServiceWorker';
import { Callout } from '@blueprintjs/core';

import './index.css';
import 'normalize.css/normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';

try {
  const backend = new window.Panopticon(window.argument);

  ReactDOM.render(
    <App backend={backend} />,
    document.getElementById('root')
  );

  if (window.isDev) {
    registerServiceWorker();
  }
} catch(e) {
  ReactDOM.render(
    <div className="Error">
    <Callout intent="danger" title="Failure">
      Disassembly process failed to start. You either did not pick a file or the file you picked cannot be recognized.
    </Callout></div>,
    document.getElementById('root')
  );
}
