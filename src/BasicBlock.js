import React from 'react';
import Mnemonic from './Mnemonic.js';

import './BasicBlock.css';

function BasicBlock({ block, style, }) {
  const mnes = block.mnemonics.map((mne) =>
    <Mnemonic key={mne.start} mnemonic={mne} baseAddress={block.start} />);

  return (
    <div className="BasicBlock" style={style}>
      <div className="BasicBlock-body">{mnes}</div>
    </div>
  );
}

export default BasicBlock;
