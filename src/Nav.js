import React from 'react';
import { EditableText, Navbar, Button, Alignment } from '@blueprintjs/core';

import './Nav.css';

class Nav extends React.PureComponent {
  render() {
    const { selectedFunc, backend } = this.props;
    let nameEdit;

    if (selectedFunc) {
      const name = selectedFunc.name;
      const uuid = selectedFunc.uuid;

      nameEdit = (
        <EditableText
          key={"nav-name-" + name}
          defaultValue={name}
          confirmOnEnterKey={true}
          selectAllOnFocus={true}
          onConfirm={(name) => backend.rename(uuid, name)}
        />
      );
    } else {
      nameEdit = <div />;
    }

    return (
      <Navbar className="Nav" fixedToTop={false} key={"app-nav"}>
        <Navbar.Group align={Alignment.LEFT} key={"nav-group-nav"}>
          <Button
            className="bp3-minimal"
            icon="chevron-left"
            onClick={() => window.history.back()}
            key={"nav-back"}
          />
        </Navbar.Group>
        <Navbar.Group align={Alignment.RIGHT} key={"nav-group-func"}>
          {nameEdit}
        </Navbar.Group>
      </Navbar>
    );
  }
}

export default Nav;
