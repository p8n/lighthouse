const net = require('net');
const port = process.env.PORT ? (process.env.PORT - 100) : 5000;

process.env.ELECTRON_START_URL = `http://localhost:${port}`;

const client = new net.Socket();

let startedElectron = false;
const tryConnection = () => client.connect({port: port}, () => {
  client.end();
  if(!startedElectron) {
    console.log('starting electron');
    startedElectron = true;

    const spawn = require('child_process').spawn;
    let proc = spawn('npm', ['run', 'electron']);

    proc.stdout.on('data', function (data) {
      console.log('stdout: ' + data.toString());
    });

    proc.stderr.on('data', function (data) {
      console.log('stderr: ' + data.toString());
    });

    proc.on('exit', function (code) {
      console.log('child process exited with code ' + code.toString());
    });
  }
});

tryConnection();

client.on('error', (error) => {
    setTimeout(tryConnection, 1000);
});
