#[macro_use]
extern crate neon;

extern crate crossbeam;
extern crate petgraph;
extern crate rand;
extern crate p8n_types as types;
extern crate p8n_analysis as analysis;
extern crate p8n_avr as avr;
extern crate p8n_amd64 as amd64;

use std::thread;
use std::sync::Arc;
use std::collections::HashMap;
use rand::{Rng, thread_rng};
use neon::prelude::*;
use crossbeam::queue::MsQueue;
use petgraph::{
    Direction,
    visit::EdgeRef,
};
use types::{
    Function, Uuid, CfgNode,
    Machine, Content, Constant, Variable,
    Guard,
};
use analysis::{
    Event, disassemble,
};

struct Line {
    is_entry: bool,
    offset: u64,
    address: u64,
    opcode: String,
    args: Vec<String>,
    // name: String,
    sources: Vec<u64>,
    targets: Vec<(u64,String)>,
}

enum Update {
    Todo{ name: String, uuid: Uuid, },
    Function{ function: Function, },
    Listing{ uuid: Uuid, lines: Vec<Line>, },
    Done{ message: String, },
    Failure{ message: String, },
}

pub struct Panopticon {
    updates: Arc<MsQueue<Update>>,
    functions: HashMap<Uuid, Function>,
    comments: HashMap<u64, String>,
    by_address: HashMap<u64, Uuid>,
}

impl Default for Panopticon {
    fn default() -> Self {
        Panopticon{
            updates: Arc::new(MsQueue::default()),
            functions: HashMap::default(),
            by_address: HashMap::default(),
            comments: HashMap::default(),
        }
    }
}

// Panopticon {
//  functions: [Function]
//
//  fn init()
//  fn applyUpdates()
//  fn fetchListing(uuid)
// }
//
// Function {
//  version: number,
//  name: String,
//  uuid: String,
//  entry: String,
//  listing: Async<[Line]>,
//  graph: Async<CtrlFlowGraph>,
// }
//
// Async<T> {
//  version: number,
//  state: String,
//  value: null / T,
// }
//
// Line {
//  version: number,
//  type: String,
//  address: String,
//
//  -- type == mnemonic --
//  opcode: String,
//  args: [String],
//  comment: String,
//
//  -- type == location --
//  name: null / String
//  sources:[String]
//
//  -- type == branch --
//  targets: [String]
// }
//
// CtrlFlowGraph {
//  ...
// }

enum Async<'a> {
    Missing,
    Running,
    Present(Handle<'a, JsValue>),
}

fn build_async<'a>(cx: &mut CallContext<'a, JsPanopticon>, value: Async<'a>)
    -> JsResult<'a, JsObject>
{
    let ret = cx.empty_object();

    match value {
        Async::Present(obj) => {
            let state = cx.string("present");
            let version = cx.number(thread_rng().gen::<f64>());
            ret.set(cx, "version", version)?;
            ret.set(cx, "state", state)?;
            ret.set(cx, "value", obj)?;
        }
        Async::Missing => {
            let state = cx.string("missing");
            ret.set(cx, "state", state)?;
        }
        Async::Running => {
            let state = cx.string("running");
            ret.set(cx, "state", state)?;
        }
    }

    Ok(ret)
}

fn build_function<'a>(cx: &mut CallContext<'a, JsPanopticon>, f: &Function)
    -> JsResult<'a, JsObject>
{
    let meta = cx.empty_object();
    let uuid = cx.string(format!("{}", f.uuid()));
    let name = cx.string(f.name.clone());
    let entry = cx.number(f.entry_address() as f64);
    let listing = build_async(cx, Async::Missing)?;

    meta.set(cx, "uuid", uuid)?;
    meta.set(cx, "name", name)?;
    meta.set(cx, "entry", entry)?;
    meta.set(cx, "listing", listing)?;

    Ok(meta)
}

fn build_listing<'a>(cx: &mut CallContext<'a, JsPanopticon>, lines: Vec<Line>,
                     uuid: &Uuid) -> JsResult<'a, JsObject>
{
    let ret = cx.empty_object();
    let ret_lines = cx.empty_array();
    let mut this = cx.this();
    let mut entry_idx = None;

    for line in lines {
        let meta = cx.empty_object();
        let next_idx = cx.number(ret_lines.len());
        let Line{
            offset, address, opcode, args: in_args,
            sources, targets, is_entry,
        } = line;

        cx.borrow_mut(&mut this, |mut session| {
            session.by_address.insert(address, uuid.clone());
        });

        if is_entry { entry_idx = Some(ret_lines.len()); }

        let is_head = cx.boolean(offset == 0);
        let opcode = cx.string(opcode);
        let comment = cx.borrow(&this, |session| -> String {
            session.comments
                .get(&address).cloned().unwrap_or_default()
        });
        let comment = cx.string(comment);
        let offset = cx.string(format!("+ {}", offset));
        let address = cx.string(format!("{:#x}", address));
        let mut args = cx.empty_array();
        let mut srcs = cx.empty_array();
        let mut tgts = cx.empty_array();
        let mut gs = cx.empty_array();

        for (v,g) in targets {
            let next_idx = cx.number(tgts.len());

            let val = cx.string(format!("{}", v));
            let guard = cx.string(g);
            tgts.set(cx, next_idx, val)?;
            gs.set(cx, next_idx, guard)?;
        }

        for s in sources {
            let next_idx = cx.number(srcs.len());

            let val = cx.string(format!("{}", s));
            srcs.set(cx, next_idx, val)?;
        }

        for arg in in_args {
            let next_idx = cx.number(args.len());

            let val = cx.string(arg);
            args.set(cx, next_idx, val)?;
        }

        meta.set(cx, "isHead", is_head)?;
        meta.set(cx, "address", address)?;
        meta.set(cx, "offset", offset)?;
        meta.set(cx, "opcode", opcode)?;
        meta.set(cx, "args", args)?;
        meta.set(cx, "comment", comment)?;
        meta.set(cx, "sources", srcs)?;
        meta.set(cx, "targets", tgts)?;
        meta.set(cx, "guards", gs)?;

        ret_lines.set(cx, next_idx, meta)?;
    }

    let entry = cx.number(entry_idx.unwrap_or(0));

    ret.set(cx, "entryPoint", entry)?;
    ret.set(cx, "lines", ret_lines)?;

    Ok(ret)
}

fn update_listing<'a>(cx: &mut CallContext<'a, JsPanopticon>, uuid: &Uuid)
    -> JsResult<'a, JsValue>
{
    let this = cx.this();
    let opt = cx.borrow(&this, |s| {
        s.functions.get(uuid).cloned().map(|func| {
            (func, s.updates.clone())
        })
    });
    let (func, queue) = match opt {
        Some((func, queue)) => (func, queue),
        None => { return cx.throw_error("argument is not a valid UUID"); }
    };

    spawn_listinglizer(queue, func);

    let uuid_str = cx.string(format!("{}", uuid));
    let async = this.get(cx, "functions")?.downcast_or_throw::<JsObject,_>(cx)?;
    let funcs = async.get(cx, "value")?.downcast_or_throw::<JsObject,_>(cx)?;
    let func = funcs.get(cx, uuid_str)?.downcast_or_throw::<JsObject,_>(cx)?;
    let async = build_async(cx, Async::Running)?;

    func.set(cx, "listing", async)?;

    Ok(cx.undefined().as_value(cx))
}

fn update_functions<'a>(cx: &mut CallContext<'a, JsPanopticon>, uuid: &Uuid,
                        meta: Handle<'a, JsObject>) -> JsResult<'a, JsValue>
{
    let this = cx.this();
    let async = this.get(cx, "functions")?.downcast_or_throw::<JsObject,_>(cx)?;
    let funcs = async.get(cx, "value")?.downcast_or_throw::<JsObject,_>(cx)?;
    let uuid = cx.string(format!("{}", uuid));

    funcs.set(cx, uuid, meta)?;

    let funcs = funcs.as_value(cx);
    let async = build_async(cx, Async::Present(funcs))?;
    this.set(cx, "functions", async)?;

    Ok(cx.undefined().as_value(cx))
}

fn spawn_disassembler(c: Content, queue: Arc<MsQueue<Update>>) {
    let entries = c.entry_points.iter().map(|p| {
        (p.offset, p.name.clone())
    }).collect::<Vec<_>>();
    let symbolic = c.symbolic.iter().map(|p| {
        (p.offset, p.name.clone())
    }).collect::<Vec<_>>();

    let callback = {
        let queue = queue.clone();

        Some(move |ev: Event| {
            match ev {
                Event::Update{ function } => {
                    queue.push(Update::Function{
                        function: function.clone(),
                    });
                }
                Event::Discover{ uuid, name,.. } =>
                    queue.push(Update::Todo{ uuid: uuid, name: name.to_string() }),
                ev => { eprintln!("Unhandled event {:?}", ev); }
            }
        })
    };

    thread::spawn(move || {
        let th = {
            let queue = queue.clone();

            thread::spawn(move || {
                let res = match c.machine {
                    Machine::Avr => disassemble::<avr::Avr, _>(
                        avr::Mcu::atmega103(), &c.segments[0],
                        &entries, &symbolic, callback),
                    Machine::Amd64 => disassemble::<amd64::Amd64, _>(
                        amd64::Mode::Long, &c.segments[0],
                        &entries, &symbolic, callback),
                    Machine::Ia32 => disassemble::<amd64::Amd64, _>(
                        amd64::Mode::Protected, &c.segments[0],
                        &entries, &symbolic, callback),
                };

                match res {
                    Ok(_) => {
                        queue.push(Update::Done{ message: "Disassembly job finished".to_string() });
                    }
                    Err(e) => {
                        queue.push(Update::Failure{ message: format!("Disassembly job failed: {}", e) });
                    }
                }
            })
        };

        match th.join() {
            Ok(()) => {}
            Err(e) => {
                queue.push(Update::Failure{ message: format!("Disassembly job panicked: {:?}", e) });
            }
        }
    });
}

fn spawn_listinglizer(queue: Arc<MsQueue<Update>>, f: Function) {
    thread::spawn(move || {
        let th = {
            let queue = queue.clone();
            thread::spawn(move || {
                let mut lines = Vec::default();
                let mut bbs = f.basic_blocks().collect::<Vec<_>>();
                let mut by_addr = HashMap::new();

                bbs.sort_by_key(|x| x.1.area.start);

                let mut idx = 0;
                for &(bb_idx, _) in bbs.iter() {
                    for (_, mne) in f.mnemonics(bb_idx) {
                        let opcode = f.strings.value(mne.opcode).unwrap();

                        if !opcode.starts_with("__") {
                            by_addr.insert(mne.area.start, idx);
                            idx += 1;
                        }
                    }
                }

                for (bb_idx, bb) in bbs {
                    let cfg = f.cflow_graph();

                    // incoming jumps
                    let sources = cfg
                        .edges_directed(bb.node, Direction::Incoming)
                        .filter_map(|e| match cfg.node_weight(e.source()) {
                            Some(&CfgNode::BasicBlock(other_idx)) => {
                                let other_bb = f.basic_block(other_idx);
                                if bb.area.start != other_bb.area.end {
                                    by_addr.get(&other_bb.area.start).map(|&x| x as u64)
                                } else {
                                    None
                                }
                            }
                            _ => None,
                        }).collect::<Vec<_>>();

                    // outgoing jumps
                    let targets = cfg
                        .edges_directed(bb.node, Direction::Outgoing)
                        .filter_map(|e| match cfg.node_weight(e.target()) {
                            Some(&CfgNode::BasicBlock(other_idx)) => {
                                let other_bb = f.basic_block(other_idx);
                                if bb.area.end != other_bb.area.start {
                                    let g = match e.weight() {
                                        Guard::True => "".to_string(),
                                        Guard::False => "never".to_string(),
                                        Guard::Predicate{ flag: Variable{ name,.. }, expected: true } => {
                                            format!("if {}", f.names.value(*name).unwrap().base())
                                        }
                                        Guard::Predicate{ flag: Variable{ name,.. }, expected: false } => {
                                            format!("if !{}", f.names.value(*name).unwrap().base())
                                        }
                                    };
                                    by_addr.get(&other_bb.area.start).map(|&x| (x as u64,g))
                                } else {
                                    None
                                }
                            }
                            _ => None,
                        }).collect::<Vec<_>>();

                    let mne_cnt = f.mnemonics(bb_idx).filter(|(_, mne)| {
                        !f.strings.value(mne.opcode).unwrap().starts_with("__")
                    }).count();
                    let iter = f.mnemonics(bb_idx).filter(|(_, mne)| {
                        !f.strings.value(mne.opcode).unwrap().starts_with("__")
                    });
                    let is_entry = f.entry_point() == bb_idx;

                    // mnemonics
                    for (pos, (_, mne)) in iter.enumerate() {
                        let opcode = f.strings.value(mne.opcode).unwrap();

                        let mut args = Vec::with_capacity(mne.operands.len());

                        for val in mne.operands.iter() {
                            match val {
                                &types::Value::Constant(Constant{ value,.. }) => {
                                    args.push(format!("{:#x}", value));
                                }
                                &types::Value::Variable(Variable{ name,.. }) => {
                                    args.push(f.names.value(name).unwrap()
                                              .base().to_string());
                                }
                                &types::Value::Undefined => {
                                    args.push("?".to_string());
                                }
                            }
                        }

                        //assert_eq!(pos, by_addr[&mne.area.start]);

                        let off = if mne.area.start >= bb.area.start {
                            mne.area.start - bb.area.start
                        } else {
                            mne.area.start
                        };
                        lines.push(Line{
                            is_entry: is_entry && pos == 0,
                            offset: off,
                            address: mne.area.start,
                            opcode: opcode.to_string(),
                            args: args,
                            sources: if pos == 0 { sources.clone() }
                                     else { vec![] },
                            targets: if pos == mne_cnt - 1 { targets.clone() }
                                     else { vec![] },
                        });
                    }
                }

                queue.push(Update::Listing{
                    uuid: f.uuid().clone(),
                    lines: lines });
            })
        };

        match th.join() {
            Ok(()) => {}
            Err(e) => {
                queue.push(Update::Failure{
                    message: format!("Listinglizer job panicked: {:?}", e)
                });
            }
        }
    });
}

declare_types! {
    pub class JsPanopticon for Panopticon {
        init(mut cx) {
            use std::path::Path;

            let path = cx.argument::<JsString>(0)?.value();
            let res = Content::load(&Path::new(&path)).and_then(|c| {
                let ret = Panopticon::default();
                spawn_disassembler(c, ret.updates.clone());

                Ok(ret)
            });

            match res {
                Ok(x) => Ok(x),
                Err(s) => cx.throw_error(format!("{}", s)),
            }
        }

        constructor(mut cx) {
            let funcs = cx.empty_object().as_value(&mut cx);
            let async = build_async(&mut cx, Async::Present(funcs))?;
            let this = cx.this().downcast_or_throw::<JsObject,_>(&mut cx)?;

            this.set(&mut cx, "functions", async)?;

            // dunno what that means
            Ok(None)
        }

        method applyUpdates(mut cx) {
            let mut this = cx.this();
            let updates_waiting = cx.borrow(&this, |session| {
                !session.updates.is_empty()
            });

            if updates_waiting {
                while let Some(up) = cx.borrow_mut(&mut this, |s| s.updates.try_pop()) {
                    match up {
                        Update::Function{ function: f } => {
                            let meta = build_function(&mut cx, &f)?;

                            update_functions(&mut cx, f.uuid(), meta)?;
                            cx.borrow_mut(&mut this, |mut s| {
                                s.functions.insert(f.uuid().clone(), f)
                            });
                        }
                        Update::Todo{ name, uuid } => {
                            let meta = cx.empty_object();
                            let uuid_str = cx.string(format!("{}", uuid));
                            let name = cx.string(name);

                            meta.set(&mut cx, "uuid", uuid_str)?;
                            meta.set(&mut cx, "name", name)?;

                            update_functions(&mut cx, &uuid, meta)?;
                        }
                        Update::Listing{ uuid, lines } => {
                            let uuid_str = cx.string(format!("{}", uuid));
                            let async = this.get(&mut cx, "functions")?
                                .downcast_or_throw::<JsObject,_>(&mut cx)?;
                            let funcs = async.get(&mut cx, "value")?
                                .downcast_or_throw::<JsObject,_>(&mut cx)?;
                            let func = funcs.get(&mut cx, uuid_str)?
                                .downcast_or_throw::<JsObject,_>(&mut cx)?;
                            let listing = build_listing(&mut cx, lines, &uuid)?
                                .as_value(&mut cx);
                            let async = build_async(&mut cx,
                                                    Async::Present(listing))?;

                            func.set(&mut cx, "listing", async)?;
                        }
                        Update::Done{ message } => {
                            eprintln!("Task done: {}", message);
                        }
                        Update::Failure{ message } => {
                            eprintln!("Task failed: {}", message);
                        }
                    }
                }

                Ok(cx.boolean(true).upcast())
            } else {
                Ok(cx.boolean(false).upcast())
            }
        }

        method fetchListing(mut cx) {
            use std::str::FromStr;

            let uuid_str = cx.argument::<JsString>(0)?;
            let uuid = match Uuid::from_str(&uuid_str.value()) {
                Ok(uu) => uu,
                Err(()) => {
                    return cx.throw_error("argument is not a valid UUID");
                }
            };

            update_listing(&mut cx, &uuid)
        }

        method comment(mut cx) {
            use std::str::FromStr;

            let addr = cx.argument::<JsString>(0)?.value();
            let res = if addr.starts_with("0x") || addr.starts_with("0X") {
                u64::from_str_radix(&addr[2..], 16)
            } else {
                u64::from_str(&addr)
            };
            let addr = match res {
                Ok(addr) => addr,
                Err(e) => {
                    return cx.throw_error(
                        format!("argument '{}' is not a valid address: {}",
                                addr, e));
                }
            };

            let cmnt = cx.argument::<JsString>(1)?.value();
            let mut this = cx.this();

            let maybe_uuid = cx.borrow_mut(&mut this, |mut s| {
                s.comments.insert(addr, cmnt);
                s.by_address.get(&addr).cloned()
            });

            match maybe_uuid {
                Some(uuid) => { update_listing(&mut cx, &uuid)?; }
                None => { /* nothing */ }
            }

            Ok(cx.undefined().as_value(&mut cx))
        }

        method rename(mut cx) {
            use std::str::FromStr;

            let uuid = cx.argument::<JsString>(0)?.value();
            let res = Uuid::from_str(&uuid);
            let uuid = match res {
                Ok(uuid) => uuid,
                Err(()) => {
                    return cx.throw_error(
                        format!("argument '{}' is not a valid uuid", uuid));
                }
            };

            let name = cx.argument::<JsString>(1)?.value();

            let mut this = cx.this();
            let maybe_f = cx.borrow_mut(&mut this, |mut s| {
                match s.functions.get_mut(&uuid) {
                    Some(ref mut f) => {
                        f.name = name.into();
                        Some(f.clone())
                    }
                    None => {
                        None
                    }
                }
            });

            match maybe_f {
                Some(f) => {
                    let meta = build_function(&mut cx, &f)?;

                    update_functions(&mut cx, f.uuid(), meta)?;
                    cx.borrow_mut(&mut this, |mut s| {
                        s.functions.insert(f.uuid().clone(), f)
                    });

                    Ok(cx.undefined().as_value(&mut cx))
                }
                None => {
                    cx.throw_error(
                        format!("unknown function: {}", uuid))
                }
            }
        }
    }
}

register_module!(mut cx, {
    cx.export_class::<JsPanopticon>("Panopticon")?;

    Ok(())
});
